### What is this repository for? ###

* This python script takes a input csv file and plots out the lat and long points with overlaying circles that represent the accuracy

### How do I get set up? ###
download all files and run it in a python environment with MapGPS.py as the main execution file

### Who do I talk to? ###

Anthony Chong
anthonymchong@gmail.com